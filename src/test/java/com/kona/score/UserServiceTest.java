package com.kona.score;

import com.kona.score.dao.entity.UserEntity;
import com.kona.score.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;


    @Test
    public void findByUsernameTest2() {

        UserEntity user = userService
                .findByUsername("sarwar");

        Assertions.assertNotNull(user);
    }


}
