package com.kona.score;

import com.kona.score.dao.entity.UserEntity;
import com.kona.score.dao.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByUsernameTest() {

        Optional<UserEntity> optional = userRepository.findByUsername("sarwar");

        Assertions.assertNotNull(optional);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    public void findByUsernameTest2() {

        Optional<UserEntity> optional = userRepository
                .findByUsername("sarwar", 1, 6, 7, 9);

        Assertions.assertNotNull(optional);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    public void update() {
        userRepository.findByUsername("sarwar").ifPresent(userEntity -> {
            userEntity.setUpdatedBy(1);
            userRepository.save(userEntity);
        });
    }
}
