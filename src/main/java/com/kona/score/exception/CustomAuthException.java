package com.kona.score.exception;

import org.springframework.security.core.AuthenticationException;

//custom authentication exception handler class
public class CustomAuthException extends AuthenticationException {

    // set custom message and show
    public CustomAuthException(String msg) {
        super(msg);
    }
}
