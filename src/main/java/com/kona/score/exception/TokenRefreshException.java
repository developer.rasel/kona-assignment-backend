package com.kona.score.exception;
// custom RuntimeException handler
public class TokenRefreshException extends RuntimeException {

    private final String code;
    // set custom message and show
    public TokenRefreshException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
