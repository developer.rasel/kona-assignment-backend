package com.kona.score.exception;

import org.springframework.dao.DataIntegrityViolationException;

//custom sql exception handler class

public class CustomSqlException extends DataIntegrityViolationException {
    // set custom message and show
    public CustomSqlException(String msg) {
        super(msg);
    }

}
