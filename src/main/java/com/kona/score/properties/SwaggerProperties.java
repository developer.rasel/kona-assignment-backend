package com.kona.score.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

// swagger property for enable and disable swagger
@Data
@Configuration
public class SwaggerProperties {

    @Value("${swagger.enable}")
    private boolean isEnable;
}
