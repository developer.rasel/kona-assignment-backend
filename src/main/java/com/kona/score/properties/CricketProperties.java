package com.kona.score.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class CricketProperties {

    // url property for read or parse data for cricket score
    @Value("${cricket.url}")
    private String cricketURL;
}
