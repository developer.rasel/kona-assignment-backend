package com.kona.score.handler;

import com.kona.score.common.ResponseStatus;
import com.kona.score.model.ResponseBody;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

// global exception handler class
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    //access denied exception handle
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
        return new ResponseEntity<>(
                "Access denied message here", new HttpHeaders(), HttpStatus.OK);
    }

    //jwt expired exception handle
    @ExceptionHandler(ExpiredJwtException.class)
    public final ResponseEntity<Object> handleJwtTokenExpiredException(ExpiredJwtException ex) {
        ResponseBody<Object> responseBody = new ResponseBody<>();
        responseBody.setIsSuccessful(false);
        responseBody.setCode(ResponseStatus.JWT_EXPIRED.getCode());
        responseBody.setMessage(ex.getMessage());
        responseBody.setData(null);
        return ResponseEntity.ok(responseBody);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        StringBuilder message = new StringBuilder("DTO validation failed.");
        message.append(System.lineSeparator());

        ex.getBindingResult().getAllErrors().forEach(error ->
                message.append(((FieldError) error).getField())
                        .append("-").append(error.getDefaultMessage())
                        .append(System.lineSeparator()));

        return getResponseEntity(message.toString());
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(
            MissingPathVariableException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        return getResponseEntity("Path variable required.");

    }

    // no handle found
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
                                                                   HttpStatus status, WebRequest request) {

        return getResponseEntity("No handler found.");

    }

    // type missmatch
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(
            TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String message = "Path variable or param value type mismatch.";

        if (ex instanceof MethodArgumentTypeMismatchException) {

            MethodArgumentTypeMismatchException exception = (MethodArgumentTypeMismatchException) ex;
            message = "Path variable or param value type mismatch for " + exception.getName();
        }

        return getResponseEntity(message);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        return getResponseEntity(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            Exception ex, Object body,
            HttpHeaders headers, HttpStatus status, WebRequest request) {

        return getResponseEntity(ex.getMessage());
    }

    // make response body

    private ResponseEntity<Object> getResponseEntity(String message) {

        ResponseBody<Object> responseBody = new ResponseBody<>();
        responseBody.setIsSuccessful(false);
        responseBody.setCode(ResponseStatus.UNSUCCESSFUL.getCode());
        responseBody.setMessage(message);
        responseBody.setData(null);
        return ResponseEntity.ok(responseBody);
    }
}