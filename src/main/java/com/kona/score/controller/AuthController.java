package com.kona.score.controller;

import com.kona.score.dao.entity.UserEntity;
import com.kona.score.exception.CustomAuthException;
import com.kona.score.common.ResponseStatus;
import com.kona.score.exception.TokenRefreshException;
import com.kona.score.model.*;
import com.kona.score.model.ResponseBody;
import com.kona.score.service.RefreshTokenService;
import com.kona.score.service.UserService;
import com.kona.score.security.JwtProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//authentication controller
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final AuthenticationManager authenticationManager;

    private final JwtProvider jwtProvider;

    private final UserService userService;

    private final RefreshTokenService refreshTokenService;

    //dependency management
    public AuthController(AuthenticationManager authenticationManager, JwtProvider jwtProvider,
                          UserService userService, RefreshTokenService refreshTokenService) {
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.userService = userService;
        this.refreshTokenService = refreshTokenService;
    }

    //login path and function
    @PostMapping("/login")
    public ResponseBody<Object> authenticate(@Valid @RequestBody LoginRequestBody requestBody) {

        logger.info("Authentication request body : {}", requestBody);

        //make response body
        ResponseBody<Object> responseBody = new ResponseBody<>();
        responseBody.setIsSuccessful(false);
        responseBody.setCode(ResponseStatus.UNSUCCESSFUL.getCode());
        responseBody.setMessage(ResponseStatus.UNSUCCESSFUL.getValue());

        //take authentication decission
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(requestBody.getUsername(), requestBody.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtProvider.generateJwtTokenByUsername(requestBody.getUsername());
            String refreshToken = refreshTokenService.createRefreshToken(requestBody.getUsername());
            UserEntity userEntity= userService.findByUsername(requestBody.getUsername());
            responseBody.setIsSuccessful(true);
            responseBody.setCode(ResponseStatus.SUCCESS.getCode());
            responseBody.setMessage("Login successful!");
            responseBody.setData(new JwtResponseBody(jwt, refreshToken));
            responseBody.setUserEntity(userEntity);
//        exception handling
        } catch (BadCredentialsException e) {
            logger.error("BadCredentialsException occurred while authenticate user.", e);
            responseBody.setCode(ResponseStatus.UNSUCCESSFUL.getCode());
            responseBody.setMessage(userService.getMessage(requestBody.getUsername()));
        } catch (CustomAuthException e) {
            logger.error("CustomAuthException occurred while authenticate user.", e);
            responseBody.setCode(ResponseStatus.UNSUCCESSFUL.getCode());
            responseBody.setMessage(e.getMessage());
        } catch (AuthenticationException e) {
            logger.error("AuthenticationException occurred while authenticate user.", e);
            responseBody.setCode(ResponseStatus.UNSUCCESSFUL.getCode());
            responseBody.setMessage(e.getMessage());
        }

        logger.info("Authentication response body : {}", responseBody);

        return responseBody;
    }

//    validate token
    @PostMapping("/validate-token")
    public ResponseBody<Object> validateToken(@Valid @RequestBody TokenValidationRequestBody requestBody) {

        logger.info("Token validation request body : {}", requestBody);

        ResponseBody<Object> responseBody = new ResponseBody<>();

        TokenValidationResponseBody result = jwtProvider.validateToken(requestBody.getToken());

        // decission making
        if (Boolean.TRUE.equals(result.getIsSuccessful())) {
            responseBody.setIsSuccessful(true);
            responseBody.setCode(ResponseStatus.SUCCESS.getCode());
            responseBody.setMessage(result.getMessage());
        } else {
            responseBody.setIsSuccessful(false);
            responseBody.setMessage(result.getMessage());
            if (Boolean.TRUE.equals(result.getIsTokenExpired())) {
                responseBody.setCode(ResponseStatus.JWT_EXPIRED.getCode());
            } else {
                responseBody.setCode(ResponseStatus.UNSUCCESSFUL.getCode());
            }
        }

        logger.info("Token validation response body : {}", responseBody);

        return responseBody;
    }

    //refresh previous token validity
    @PostMapping("/refresh-token")
    public ResponseBody<Object> refreshToken(@Valid @RequestBody RefreshTokenRequestBody requestBody) {

        logger.info("Refresh token request body : {}", requestBody);

        ResponseBody<Object> responseBody = new ResponseBody<>();
        responseBody.setIsSuccessful(false);
        responseBody.setCode(ResponseStatus.UNSUCCESSFUL.getValue());
        responseBody.setMessage("Invalid request.");

        try {

            JwtResponseBody data = refreshTokenService.refreshToken(requestBody.getRefreshToken());
            responseBody.setIsSuccessful(true);
            responseBody.setCode(ResponseStatus.SUCCESS.getCode());
            responseBody.setMessage("Token refreshed successfully.");
            responseBody.setData(data);
        } catch (TokenRefreshException e) {
            responseBody.setCode(e.getCode());
            responseBody.setMessage(e.getMessage());
        }

        logger.info("Refresh token response body : {}", responseBody);

        return responseBody;
    }


}