package com.kona.score.controller;

import com.kona.score.dao.entity.LiveScore;
import com.kona.score.dao.repository.LiveScoreRepository;
import com.kona.score.service.LiveScoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final LiveScoreService liveScoreService;
    private final LiveScoreRepository liveScoreRepository;

    public DashboardController(LiveScoreService liveScoreService,LiveScoreRepository liveScoreRepository){
        this.liveScoreService=liveScoreService;
        this.liveScoreRepository=liveScoreRepository;
    }

    @ResponseBody
    @GetMapping("/livescore")
    public Page<LiveScore> getLiveScoreData(@RequestParam("draw") int draw, @RequestParam("start") int start,
                                            @RequestParam("length") int length, @RequestParam("search") String search) {
        logger.info("call from dashboard for searching:{}","draw:"+draw+",start:"+start+",length:"+length+",search:"+search);
        logger.info("get data from dashboard:{}",liveScoreService.getLiveScoreData(draw, start,length, search).getContent());
        return liveScoreService.getLiveScoreData(draw, start,length, search);
    }

    @ResponseBody
    @GetMapping("/testlivescore")
    public List<LiveScore> getTestLiveScoreData() {
        logger.info("call from dashboard for test searching:{}");
        return liveScoreRepository.findAll();
    }


}
