package com.kona.score.config;

import com.kona.score.common.Constants;
import com.kona.score.utils.Utils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.UUID;

//logger implementation class
@Data
@EqualsAndHashCode(callSuper = true)
@Component
public class Slf4jMDCFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String responseHeader = Constants.Headers.responseId;

    private final String mdcTokenKey = Constants.Headers.correlationId;

    private final String requestHeader = Constants.Headers.requestId;

    //request filering function
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        logger.debug("Requested url: {}, headers: {}", request.getRequestURL(), getHeaders(request));

        try {
            final String token;
            if (!Utils.isEmpty(requestHeader) && !Utils.isEmpty(request.getHeader(requestHeader))) {
                token = request.getHeader(requestHeader);
            } else {
                token = UUID.randomUUID().toString().toUpperCase().replace("-", "");
            }
            MDC.put(mdcTokenKey, token);
            if (!Utils.isEmpty(responseHeader)) {
                response.addHeader(responseHeader, token);
            }
            filterChain.doFilter(request, response);
        } finally {
            MDC.remove(mdcTokenKey);
        }

    }

    //make common header
    private String getHeaders(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();

        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = headerNames.nextElement();
            String value = request.getHeader(key);
            builder.append(key).append(": ").append(value).append(System.lineSeparator());
        }

        return builder.toString();
    }
}
