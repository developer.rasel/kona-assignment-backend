package com.kona.score.factory;

import com.kona.score.enums.ScoreType;
import com.kona.score.service.LiveScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

// factory pattern implementation class
@Service
public class LiveScoreFactory {

	private final HashMap<String, LiveScoreService> liveScoreServiceMap = new HashMap<>();

	//dependency injection manage
	@Autowired
	public LiveScoreFactory(List<LiveScoreService> liveScoreServiceList){
		for(LiveScoreService liveScoreService : liveScoreServiceList){
			liveScoreServiceMap.put(liveScoreService.getId(), liveScoreService);
		}
	}

	//function for choosing service engine dynamically
	public LiveScoreService getLiveScoreService(ScoreType scoreType) {

		LiveScoreService liveScoreService = null;
		switch (scoreType) {
			case CRICKET:
				return liveScoreServiceMap.get(ScoreType.CRICKET.name());
			default:
				liveScoreService = null;
		}

		return liveScoreService;
	}
}