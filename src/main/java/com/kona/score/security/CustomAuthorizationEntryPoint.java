package com.kona.score.security;

import com.kona.score.common.Constants;
import com.kona.score.common.ResponseStatus;
import com.kona.score.model.ResponseBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthorizationEntryPoint implements AuthenticationEntryPoint {

    private final ObjectMapper objectMapper;

    public CustomAuthorizationEntryPoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException {

        ResponseBody<Object> responseBody = new ResponseBody<>();
        responseBody.setIsSuccessful(false);

        if (request.getAttribute(Constants.JWT.EXPIRATION_KEY) != null) {

            responseBody.setCode(ResponseStatus.JWT_EXPIRED.getCode());
            responseBody.setMessage("Please login again or refresh jwt token.");
        } else {
            responseBody.setCode(ResponseStatus.UNAUTHORIZED.getCode());
            responseBody.setMessage(e.getMessage());
        }

        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(objectMapper.writeValueAsString(responseBody));
    }

}