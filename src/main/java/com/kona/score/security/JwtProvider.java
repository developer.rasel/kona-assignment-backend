package com.kona.score.security;

import com.kona.score.common.Constants;
import com.kona.score.model.TokenValidationResponseBody;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

@Component
public class JwtProvider {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${jwt.token.secret}")
    private String jwtSecret;

    @Value("${jwt.token.timeout.in.millisecond}")
    private long jwtExpiration;

    public String generateJwtTokenByUsername(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(Calendar.getInstance().getTime())
                .setExpiration(Date.from(Instant.ofEpochMilli(System.currentTimeMillis() + jwtExpiration)))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getUsernameFromJwtToken(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

    public void validateJwtToken(String authToken, HttpServletRequest request) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
        } catch (ExpiredJwtException e) {
            request.setAttribute(Constants.JWT.EXPIRATION_KEY, e.getMessage());
        }

    }

    public TokenValidationResponseBody validateToken(String token) {

        TokenValidationResponseBody responseBody = new TokenValidationResponseBody();
        responseBody.setIsSuccessful(false);
        responseBody.setIsTokenExpired(false);
        responseBody.setMessage("JWT is invalid.");

        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            responseBody.setIsSuccessful(true);
            responseBody.setMessage("JWT is valid.");
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature.", e);
            responseBody.setMessage("Invalid JWT signature.");
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token", e);
            responseBody.setMessage("Invalid JWT token.");
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token.", e);
            responseBody.setMessage("Expired JWT token.");
            responseBody.setIsTokenExpired(true);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token.", e);
            responseBody.setMessage("Unsupported JWT token.");
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty.", e);
            responseBody.setMessage("JWT claims string is empty.");
        } catch (Exception e) {
            logger.error("Exception occurred while validate jwt.", e);
            responseBody.setMessage(e.getMessage());
        }

        return responseBody;
    }
}