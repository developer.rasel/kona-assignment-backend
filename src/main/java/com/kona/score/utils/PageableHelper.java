package com.kona.score.utils;

import com.kona.score.model.DataTable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class PageableHelper {
    public Pageable getPageable(int start, int length) {
        int page = start / length;
        Pageable pageable = PageRequest.of(
                page,
                length
        ) ;
        return pageable;
    }

    public DataTable getDataTableData(Page<?> responseData, int draw, int start) {

        DataTable dataTable = new DataTable();
        dataTable.setData(responseData.getContent());
        dataTable.setRecordsTotal(responseData.getTotalElements());
        dataTable.setRecordsFiltered(responseData.getTotalElements());
        dataTable.setDraw(draw);
        dataTable.setStart(start);

        return dataTable;
    }
}
