package com.kona.score.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

//common utility functions
public class Utils {

    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

    public static String getDateInAmPmFormat(Date date) {
        if (date != null) {
            return new SimpleDateFormat("dd-MM-yyyy hh:mm a").format(date);
        }
        return null;
    }
}
