package com.kona.score.model;

import lombok.Data;

@Data
public class JwtResponseBody {
    private String token;

    private String type = "Bearer";

    private String refreshToken;

    public JwtResponseBody(String accessToken) {
        this.token = accessToken;
    }

    public JwtResponseBody(String token, String refreshToken) {
        this.token = token;
        this.refreshToken = refreshToken;
    }
}