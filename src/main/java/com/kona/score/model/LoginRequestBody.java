package com.kona.score.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class LoginRequestBody implements Serializable {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    @Override
    public String toString() {
        return "LoginRequestBody{" +
                "username='" + username + '\'' +
                '}';
    }
}