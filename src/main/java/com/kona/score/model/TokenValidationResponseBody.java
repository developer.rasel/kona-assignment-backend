package com.kona.score.model;

import lombok.Data;

@Data
public class TokenValidationResponseBody {

    private Boolean isSuccessful;

    private Boolean isTokenExpired;

    private String message;
}
