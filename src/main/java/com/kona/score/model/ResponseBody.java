package com.kona.score.model;

import com.kona.score.dao.entity.UserEntity;
import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseBody<T> implements Serializable {

    private Boolean isSuccessful;

    private String code;

    private String message;

    private T data;

    private UserEntity userEntity;
}