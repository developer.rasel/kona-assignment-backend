package com.kona.score.model;

import lombok.Data;

import java.util.List;

@Data
public class DataTable<T> {

    private int draw;
    private int start;
    private long recordsTotal;
    private long recordsFiltered;
    private List<T> data;
}