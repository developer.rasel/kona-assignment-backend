package com.kona.score.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class RefreshTokenRequestBody implements Serializable {

    @NotBlank
    private String refreshToken;
}
