package com.kona.score.service;

import com.kona.score.dao.entity.LiveScore;
import com.kona.score.dao.repository.LiveScoreRepository;
import com.kona.score.exception.CustomAuthException;
import com.kona.score.exception.CustomSqlException;
import com.kona.score.model.Item;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

// service for save score data to db
@Service
public class LiveDataService {
    private LiveScoreRepository liveScoreRepository;
    public LiveDataService(LiveScoreRepository liveScoreRepository){
        this.liveScoreRepository=liveScoreRepository;
    }

    public Integer saveScoreToDB(List<Item> itemList){
        Integer result=0;
        if (itemList!=null && itemList.size()>0){
            result=saveToDB(itemList);
        }
        return result;
    }

    private Integer saveToDB(List<Item> itemList) {
        Integer result=0;
        for (Item item: itemList){
            LiveScore liveScore=new LiveScore();
            liveScore.setTitle(item.getTitle());
            liveScore.setDescription(item.getDescription());
            liveScore.setGuid(item.getGuid());
            liveScore.setLink(item.getLink());
            liveScore.setCreateDate(new Date());
            try {
                liveScoreRepository.save(liveScore);
                result=1;
            } catch (DataIntegrityViolationException e) {
                throw new CustomSqlException("Duplicate data found");
            }

        }
        return result;
    }
}
