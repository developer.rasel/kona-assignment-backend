package com.kona.score.service;

import com.kona.score.common.ResponseStatus;
import com.kona.score.dao.entity.RefreshTokenEntity;
import com.kona.score.dao.repository.RefreshTokenRepository;
import com.kona.score.exception.TokenRefreshException;
import com.kona.score.model.JwtResponseBody;
import com.kona.score.security.JwtProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {

    private final long refreshTokenDurationMs;

    private final RefreshTokenRepository refreshTokenRepository;

    private final JwtProvider jwtProvider;

    public RefreshTokenService(@Value("${jwt.refresh.duration.in.millisecond}") long refreshTokenDurationMs,
                               RefreshTokenRepository refreshTokenRepository, JwtProvider jwtProvider) {
        this.refreshTokenDurationMs = refreshTokenDurationMs;
        this.refreshTokenRepository = refreshTokenRepository;
        this.jwtProvider = jwtProvider;
    }

    private Optional<RefreshTokenEntity> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public String createRefreshToken(String username) {

        RefreshTokenEntity refreshToken = new RefreshTokenEntity();
        refreshToken.setUsername(username);
        refreshToken.setExpiryDate(LocalDateTime.ofInstant(Instant.now().plusMillis(refreshTokenDurationMs), ZoneId.systemDefault()));
        refreshToken.setToken(UUID.randomUUID().toString());
        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken.getToken();
    }

    public boolean isValid(RefreshTokenEntity token) {

        return token.getExpiryDate().compareTo(LocalDateTime.now()) >= 0;
    }

    public JwtResponseBody refreshToken(String token) {

        Optional<RefreshTokenEntity> optional = findByToken(token);

        if (optional.isPresent()) {

            if (isValid(optional.get())) {

                String jwt = jwtProvider.generateJwtTokenByUsername(optional.get().getUsername());
                String refreshToken = createRefreshToken(optional.get().getUsername());
                return new JwtResponseBody(jwt, refreshToken);

            } else {
                throw new TokenRefreshException(ResponseStatus.REFRESH_TOKEN_EXPIRED.getCode(),
                        "Refresh token is expired. Please login for further process.");
            }

        } else {
            throw new TokenRefreshException(ResponseStatus.REFRESH_TOKEN_NOT_FOUND.getCode(),
                    "Refresh token not found. Please login for further process or request with valid refresh token.");
        }
    }

    @Transactional
    public void deleteByUserId(String username) {
        refreshTokenRepository.findByUsername(username).ifPresent(refreshTokenRepository::delete);
    }
}