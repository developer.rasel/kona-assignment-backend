package com.kona.score.service;

import com.kona.score.dao.entity.LiveScore;
import com.kona.score.dao.repository.LiveScoreRepository;
import com.kona.score.enums.ScoreType;
import com.kona.score.model.Item;
import com.kona.score.model.Rss;
import com.kona.score.properties.CricketProperties;
import com.kona.score.utils.PageableHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// service for retrieve Data from url
@Service
public class CricketLiveScoreService implements LiveScoreService {

    private CricketProperties cricketProperties;

    private RestTemplate restTemplate;


    private PageableHelper pageableHelper;
    private LiveScoreRepository liveScoreRepository;

    @Autowired
    public CricketLiveScoreService(CricketProperties cricketProperties, RestTemplate restTemplate,PageableHelper pageableHelper,LiveScoreRepository liveScoreRepository){
        this.cricketProperties = cricketProperties;
        this.restTemplate = restTemplate;
        this.pageableHelper=pageableHelper;
        this.liveScoreRepository=liveScoreRepository;
    }

    @Override
    public List<Item> retrieveData() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));

        // parse data from url and mapping with data model
        ResponseEntity<Rss> rss = restTemplate.exchange(
                cricketProperties.getCricketURL(),
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Rss.class
        );
        if(rss != null && rss.getBody()!=null && rss.getBody().getChannel() != null){
            //return item list
            return rss.getBody().getChannel().getItemList();
        }
        return new ArrayList<>();
    }

    @Override
    public String getId() {
        return ScoreType.CRICKET.name();
    }

    @Override
    public Page<LiveScore> getLiveScoreData(int draw, int start,int length, String search) {

        Pageable pageable = pageableHelper.getPageable(start, length);
        Page<LiveScore> responseData = null;
        if(search.trim().equals("")) {
            responseData = getAll(pageable);
        }
        else {
            responseData = getAllBySearch(pageable,search);
        }


        return responseData;
    }


    private Page<LiveScore> getAll(Pageable pageable) {
        return liveScoreRepository.findAll(pageable);
    }

    private Page<LiveScore> getAllBySearch(Pageable pageable, String search) {
        return liveScoreRepository.findAllByTitleContainingOrDescriptionContainingOrGuidContainingOrLinkContaining(search,search,search,search,pageable);
    }

}
