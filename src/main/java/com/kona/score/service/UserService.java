package com.kona.score.service;

import com.kona.score.dao.entity.UserEntity;
import com.kona.score.dao.repository.UserRepository;
import com.kona.score.exception.CustomAuthException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Optional;

@Service
public class UserService {

    private final int maxWrongLoginTry;

    private final int unlockWrongAttemptMinute;

    private final UserRepository userRepository;

    public UserService(@Value("${login.maxWrongLoginTry}") int maxWrongLoginTry,
                       @Value("${login.unlockWrongAttemptMinute}") int unlockWrongAttemptMinute,
                       UserRepository userRepository) {
        this.maxWrongLoginTry = maxWrongLoginTry;
        this.unlockWrongAttemptMinute = unlockWrongAttemptMinute;
        this.userRepository = userRepository;
    }

    public UserEntity findByUsername(String username) {

        Optional<UserEntity> optional = userRepository.findByUsername(username, 1, 6, 7, 9);

        if (optional.isPresent()) {

            UserEntity user = optional.get();

            if (user.getEnabled() == 2) {
                throw new CustomAuthException("User not found by this username/email");
            }

            // check, is user deactivated?
            if (user.getStatus() == 6 || user.getStatus() == 9) {
                throw new CustomAuthException("You are temporarily deactivated!");
            }

            // check, wrong attempt and minutes
            long currentTime = Calendar.getInstance().getTimeInMillis();
            long lastLoginTry = 0;


            long calculateMinute = (currentTime - lastLoginTry) / 60000;

            return user;

        } else {
            throw new CustomAuthException("User not found by this username/email");
        }
    }

    public void updateUserAfterLoginTry(String username, boolean isLoginSuccessful) {

        userRepository.findByUsername(username).ifPresent(user -> {
            userRepository.save(user);
        });
    }

    public void resetLoginWrongAttempt(String username) {

        userRepository.findByUsername(username).ifPresent(userEntity -> {
            userRepository.save(userEntity);
        });
    }

    public String getMessage(String username) {

        String message = "Invalid username or password. Please try with correct combination.";
        return message;
    }
}
