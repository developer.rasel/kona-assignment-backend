package com.kona.score.service;


import com.kona.score.dao.entity.LiveScore;
import com.kona.score.model.Item;
import org.springframework.data.domain.Page;

import java.util.List;

// factory interface
public interface LiveScoreService {
    List<Item> retrieveData();

    String getId();

    Page<LiveScore> getLiveScoreData(int draw, int start,int length, String search);
}
