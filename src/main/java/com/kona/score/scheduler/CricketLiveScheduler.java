package com.kona.score.scheduler;

import com.kona.score.enums.ScoreType;
import com.kona.score.factory.LiveScoreFactory;
import com.kona.score.model.Item;
import com.kona.score.service.LiveDataService;
import com.kona.score.service.LiveScoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

// sceduler for run job to fetch and save data from url
@Service
public class CricketLiveScheduler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final LiveScoreFactory liveScoreFactory;
    private final LiveDataService liveDataService;

    //dependency manage
    public CricketLiveScheduler(LiveScoreFactory liveScoreFactory, LiveDataService liveDataService){
        this.liveScoreFactory = liveScoreFactory;
        this.liveDataService = liveDataService;
    }

    // schedule job functionality
    @Scheduled(fixedDelayString = "${cricket.delay}")
    private void schedule(){
        LiveScoreService service = liveScoreFactory.getLiveScoreService(ScoreType.CRICKET);
        if(service != null){
            //retrieve Data from url
            List<Item> itemList = service.retrieveData();
            // check has there any item or not
            if(!itemList.isEmpty()){
                // save live score data to db
                if (liveDataService.saveScoreToDB(itemList)==1){
                    logger.info("Save data to db successfully");
                }
                else {
                    logger.info("Duplicate data found!");
                }
            }
        }
    }
}
