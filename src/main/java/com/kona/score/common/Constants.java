package com.kona.score.common;

//common request header object
public interface Constants {

    interface Headers {

        String requestId = "request-id";
        String responseId = "response-id";
        String correlationId = "correlation-id";
    }

    interface JWT {

        String EXPIRATION_KEY = "JWT_EXPIRED";

    }
}
