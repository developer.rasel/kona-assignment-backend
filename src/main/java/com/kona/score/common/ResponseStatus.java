package com.kona.score.common;

//common response template
public enum ResponseStatus {

    SUCCESS("200", "Success"),
    UNSUCCESSFUL("301", "Failed"),
    JWT_EXPIRED("302", "JWT is expired."),
    REFRESH_TOKEN_EXPIRED("303", "Refresh token is expired."),
    REFRESH_TOKEN_NOT_FOUND("404", "Refresh token not found."),
    UNAUTHORIZED("415", "Unauthorized.");

    private final String code;
    private final String value;

    ResponseStatus(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}