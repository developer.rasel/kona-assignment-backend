package com.kona.score;


import com.kona.score.dao.entity.UserEntity;
import com.kona.score.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Date;


@SpringBootApplication
@EnableWebMvc
@EnableScheduling
@EnableWebSecurity
public class LiveScoreApplication {


	public static void main(String[] args) {
		SpringApplication.run(LiveScoreApplication.class, args);
	}


	@Bean
	public RestTemplate getRestTemplate(){
		return new RestTemplate();
	}

}
