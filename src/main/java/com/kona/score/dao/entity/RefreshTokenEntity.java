package com.kona.score.dao.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "REFRESH_TOKEN")
@Data
public class RefreshTokenEntity {

    @Id
    @Column(name = "USERNAME", nullable = false)
    private String username;

    @Column(name = "TOKEN", nullable = false, unique = true)
    private String token;

    @Column(name = "EXPIRE_DATE", nullable = false)
    private LocalDateTime expiryDate;
}