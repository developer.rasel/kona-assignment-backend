package com.kona.score.dao.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "users")
@Data
public class UserEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "username", unique = true,
            nullable = false, length = 45)
    private String username;

    @Column(name = "password",
            nullable = false, length = 60)
    private String password;

    @Column(name = "fullname",
            nullable = false, length = 100)
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "CONTACT_NO")
    private String phone;

    @Column(name = "createdate")
    private Date createDate;

    @Column(name = "enabled", nullable = false)
    private Integer enabled;

    @Column(name = "STATUS")
    private Integer status;


    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "UPDATED_BY")
    private Integer updatedBy;

    @Column(name = "UPDATED_DATE")
    private Timestamp updatedDate;

}
