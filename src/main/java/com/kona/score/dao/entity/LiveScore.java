package com.kona.score.dao.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "LIVE_SCORE")
public class LiveScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "guid", unique = true,
            nullable = false)
    private String guid;

    @Column(name = "link")
    private String link;

    @Column(name = "createdate")
    private Date createDate;

    @Column(name = "updatedate")
    private Date updateDate;


}
