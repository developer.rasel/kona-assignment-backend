package com.kona.score.dao.repository;

import com.kona.score.dao.entity.LiveScore;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LiveScoreRepository extends JpaRepository<LiveScore,Integer> {
    Optional<LiveScore> findByGuid(String integer);

    Page<LiveScore> findAllByTitleContainingOrDescriptionContainingOrGuidContainingOrLinkContaining(String title,String description,String guid,String link, Pageable pageable);
}
