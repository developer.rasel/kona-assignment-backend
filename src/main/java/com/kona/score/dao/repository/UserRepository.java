package com.kona.score.dao.repository;


import com.kona.score.dao.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    @Query(value = "SELECT * FROM USERS WHERE USERNAME=:username AND " +
            "(STATUS=:status1 OR STATUS=:status2 OR STATUS=:status3 OR STATUS=:status4)",
            nativeQuery = true)
    Optional<UserEntity> findByUsername(
            @Param("username") String username, @Param("status1") Integer status1, @Param("status2") Integer status2,
            @Param("status3") Integer status3, @Param("status4") Integer status4);


    @Query(value = "SELECT * FROM USERS WHERE USERNAME=:username",
            nativeQuery = true)
    Optional<UserEntity> findByUsername(@Param("username") String username);
}
